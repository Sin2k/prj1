<!DOCTYPE html>
<html>

<head>
	<title>Đồ án 1: SHOESVN</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet/less" type="text/css" href="./src/public/css/client/reset.less" media="all" />
	<link rel="stylesheet/less" type="text/css" href="./src/public/css/client/base.less" media="all" />
	<link rel="stylesheet/less" type="text/css" href="./src/public/css/client/header.less" media="all" />
	<!-- <link rel="stylesheet" type="text/css" href="./src/public/css/client/header.css" media="all" /> -->
	<link rel="stylesheet" type="text/css" href="./src/public/css/client/content.css" media="all" />
	<link rel="stylesheet/less" type="text/css" href="./src/public/css/client/grid.less" media="all" />
	<link rel="stylesheet/less" type="text/css" href="./src/public/css/client/responsive.less" media="all" />
	<link rel="stylesheet" type="text/css" href="./src/public/css/client/footer.css" media="all" />
	<script src="https://kit.fontawesome.com/4ca587dd72.js" crossorigin="anonymous"></script>

	<style>
		.loader-wrapper {
			position: fixed;
			width: 100%;
			height: 100vh;
			display: flex;
			flex-direction: column;
			justify-content: center;
			align-items: center;
			background: #001300;
			animation: changeColor 10s linear infinite;
			z-index: 100;
			opacity: 70%;
		}

		.loader-wrapper .loader {
			position: relative;
			display: flex;
		}

		.loader-wrapper h2 {
			color: #00ff0a;
			font-weight: 300;
			letter-spacing: 10px;
		}

		.loader-wrapper .loader .dot {
			position: relative;
			display: block;
			width: 20px;
			height: 20px;
			background: #00ff0a;
			box-shadow: 0 0 10px #00ff0a, 0 0 20px #00ff0a, 0 0 40px #00ff0a,
				0 0 60px #00ff0a, 0 0 80px #00ff0a, 0 0 100px #00ff0a;
			margin: 20px 10px;
			transform: scale(0.1);
			border-radius: 50%;
			animation: animateDot 2s linear infinite;
			animation-delay: calc(0.1s * var(--i));
		}

		.loader-wrapper .loader:last-child .dot {
			animation-delay: calc(-0.1s * var(--i));
		}

		@keyframes animateDot {
			0% {
				transform: scale(0.1);
			}

			10% {
				transform: scale(1);
			}

			50%,
			100% {
				transform: scale(0.1);
			}
		}

		@keyframes changeColor {
			0% {
				filter: hue-rotate(0deg);
			}

			100% {
				filter: hue-rotate(360deg);
			}
		}
	</style>
</head>

<body>
	<?php session_start() ?>

	<div class="loader-wrapper">
		<div class="loader">
			<div class="dot" style="--i:0;"></div>
			<div class="dot" style="--i:1;"></div>
			<div class="dot" style="--i:2;"></div>
			<div class="dot" style="--i:3;"></div>
			<div class="dot" style="--i:4;"></div>
			<div class="dot" style="--i:5;"></div>
			<div class="dot" style="--i:6;"></div>
			<div class="dot" style="--i:7;"></div>
			<div class="dot" style="--i:8;"></div>
			<div class="dot" style="--i:9;"></div>
		</div>
		<h2>LOADING...</h2>
		<div class="loader">
			<div class="dot" style="--i:0;"></div>
			<div class="dot" style="--i:1;"></div>
			<div class="dot" style="--i:2;"></div>
			<div class="dot" style="--i:3;"></div>
			<div class="dot" style="--i:4;"></div>
			<div class="dot" style="--i:5;"></div>
			<div class="dot" style="--i:6;"></div>
			<div class="dot" style="--i:7;"></div>
			<div class="dot" style="--i:8;"></div>
			<div class="dot" style="--i:9;"></div>
		</div>
	</div>

	<div id="container">
		<?php require_once('./src/config/db/connect.php'); ?>
		<?php require_once('./src/resources/views/header.php'); ?>

		<?php
		if (isset($_GET['m'])) {
			$manage = $_GET['m'];
		} else {
			$manage = "";
		}

		if ($manage == '') {
			include './src/resources/views/main.php';
		} else if ($manage == 'login') {
			include './src/resources/views/login.php';
		} else if ($manage == 'forgot') {
			include './src/resources/views/user/forgot.php';
		} else if ($manage == 'reset_password') {
			include './src/resources/views/user/reset_password.php';
		} else if ($manage == 'account') {
			include './src/resources/views/account/account.php';
		} else if ($manage == 'update_account') {
			include './src/resources/views/account/update_account.php';
		} else if ($manage == 'orders') {
			include './src/resources/views/account/orders.php';
		} else if ($manage == 'recently_view') {
			include './src/resources/views/account/recently_view.php';
		} else if ($manage == 'register') {
			include './src/resources/views/register.php';
		} else if ($manage == 'blog') {
			include './src/resources/views/blog/blog.php';
		} else if (($manage == 'blog_detail')) {
			include './src/resources/views/blog/blog_detail.php';
		} else if (($manage == 'all_product')) {
			include './src/resources/views/product/all_product.php';
		} else if (($manage == 'product')) {
			include './src/resources/views/product/product.php';
		} else if (($manage == 'product1')) {
			include './src/resources/views/product/product1.php';
		} else if (($manage == 'product2')) {
			include './src/resources/views/product/product2.php';
		} else if (($manage == 'product3')) {
			include './src/resources/views/product/product3.php';
		} else if (($manage == 'view_gio_hang')) {
			include './src/resources/views/cart/view_gio_hang.php';
		} else if (($manage == 'view_product_detail')) {
			include './src/resources/views/cart/view_product_detail.php';
		} else if (($manage == 'checkout')) {
			include './src/resources/views/cart/checkout.php';
		} else if (($manage == 'privacing')) {
			include './src/resources/views/faq/privacing.php';
		} else if (($manage == 'purchasing')) {
			include './src/resources/views/faq/purchasing.php';
		} else if (($manage == 'shipping')) {
			include './src/resources/views/faq/shipping.php';
		} else if (($manage == 'warranting')) {
			include './src/resources/views/faq/warranting.php';
		} else if (($manage == 'map')) {
			include './src/resources/views/map.php';
		} else {
			include './src/resources/views/404.php';
		}
		?>


		<?php require_once('./src/resources/views/footer.php'); ?>

		<script type="text/javascript">
			window.onload = function() {
				setTimeout(function() {
					var loader = document.getElementsByClassName("loader-wrapper")[0];
					loader.style.display = "none";
				}, 500)
			}
		</script>

		<script src="./src/public/js/slide.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/less@4"></script>
	</div>

</body>

</html>