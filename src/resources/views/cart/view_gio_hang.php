<div class="main-view-cart">
	<div class="wrap">
		<?php
		if (isset($_SESSION["cart"]) && count($_SESSION["cart"]) > 0) {
			$arr_gio_hang = $_SESSION["cart"];
		?>
			<div class="breadcrumb iiii">
				<ul class="breadcrumbs">
					<li><a href="index.php">Trang chủ<i class="fas fa-home"></i></a></li>
					<li><span>Giỏ hàng</span></li>
				</ul>
			</div>
			<div style="width: 97%; margin: auto;">
			<div class="side-bar">
				<?php
				$sql_danhmuc = "select * from danh_muc";
				$result = mysqli_query($con, $sql_danhmuc);
				?>
				<div class="category">
					<div class="healing-category">
						<div class="healing-category-img">
							<img src="src/public/images/menu.png" width="30px" height="30px">
						</div>
						<div class="healing-category-txt">
							<p>
								Danh mục sản phẩm
							</p>
						</div>
					</div>
					<ul id="menutree">
						<?php
						while ($row_danhmuc = mysqli_fetch_array($result)) {
						?>
							<?php if ($row_danhmuc['ma_danh_muc'] == 1) { ?>
								<li class="collapse">
									<input type="checkbox" id="m0" />
									<label for="m0">
										<a href="?m=product1&category=1"><?php echo $row_danhmuc['ten_danh_muc'] ?></a>
									</label>
									<ul>
										<li class="collapse">
											<input type="checkbox" id="m1" />
											<label for="m1">
												<a href="?m=product2&category=1&id=3">Adidas</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=1&id=3&type=5">Ultra Boost</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=3&type=6">Alphabounce</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=3&type=7">Stan Smith</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=3&type=8">Yeezy</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m2" />
											<label for="m2">
												<a href="?m=product2&category=1&id=2">Nike</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=1&id=2&type=1">Air Max</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=2&type=2">Air Force 1</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=2&type=3">Air Zoom</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=2&type=4">Air Jordan 1</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m3" />
											<label for="m3">
												<a href="?m=product2&category=1&id=4">Vans</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=1&id=4&type=9">Old Skool</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=4&type=10">SK8-Hi</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m4" />
											<label for="m4">
												<a href="?m=product2&category=1&id=5">Puma</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=1&id=5&type=11">Smash</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=5&type=12">Muse X</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li>
											<a href="?m=product2&category=1&id=1">New Balance</a>
											<span class="arw"></span>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m5" />
											<label for="m5">
												<a href="?m=product2&category=1&id=6">Converse</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=1&id=6&type=13">Jack Purcell</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=6&type=14">Chuck Taylor</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
									</ul>
								</li>
							<?php } else if ($row_danhmuc['ma_danh_muc'] == 2) { ?>
								<li class="collapse">
									<input type="checkbox" id="m6" />
									<label for="m6">
										<a href="?m=product1&category=2"><?php echo $row_danhmuc['ten_danh_muc'] ?></a>
									</label>
									<ul>
										<li class="collapse">
											<input type="checkbox" id="m7" />
											<label for="m7">
												<a href="?m=product2&category=2&id=3">Adidas</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=2&id=3&type=5">Ultra Boost</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=3&type=6">Alphabounce</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=3&type=7">Stan Smith</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=3&type=8">Yeezy</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m8" />
											<label for="m8">
												<a href="?m=product2&category=2&id=2">Nike</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=2&id=2&type=1">Air Max</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=2&type=2">Air Force 1</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=2&type=3">Air Zoom</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=2&type=4">Air Jordan 1</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m9" />
											<label for="m9">
												<a href="?m=product2&category=2&id=4">Vans</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=2&id=4&type=9">Old Skool</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=4&type=10">SK8-Hi</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m10" />
											<label for="m10">
												<a href="?m=product2&category=2&id=5">Puma</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=2&id=5&type=11">Smash</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=5&type=12">Muse X</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li>
											<a href="?m=product2&category=2&id=1">New Balance</a>
											<span class="arw"></span>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m11" />
											<label for="m11">
												<a href="?m=product2&category=2&id=6">Converse</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=2&id=6&type=13">Jack Purcell</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=6&type=14">Chuck Taylor</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
									</ul>
								</li>
							<?php } else { ?>
						<?php }
						} ?>
					</ul>
				</div>
				<div class="search-price-product">
					<div class="title-price">
						<p>Giá tiền</p>
					</div>
					<form class="form-price" action="search.php" method="get" onsubmit="return checkPrice()">
						<input type="hidden" name="search" />
						<select name="price" id="select_price">
							<option value="0"> Giá sản phẩm </option>
							<option value="Dưới 1.000.000₫">Dưới 1.000.000 ₫</option>
							<option value="1.000.000₫ - 3.000.000₫">1.000.000 ₫ - 3.000.000 ₫</option>
							<option value="3.000.000₫ - 5.000.000₫">3.000.000 ₫ - 5.000.000 ₫</option>
							<option value="5.000.000₫ - 8.000.000₫">5.000.000 ₫ - 8.000.000 ₫</option>
							<option value="8.000.000₫ - 10.000.000₫">8.000.000 ₫ - 10.000.000 ₫</option>
							<option value="Trên 10.000.000₫">Trên 10.000.000 ₫</option>
						</select>
						<span class="fix-price" id="err_select_price"></span>
						<button type="submit" class="btn-tk">Tìm kiếm</button>
					</form>
				</div>
				<div class="news-product">
					<div class="title-news">
						<p>Bài viết mới nhất</p>
						<span></span>
					</div>
					<div class="content-news">
						<?php
						$sql = "SELECT * FROM tin_tuc ORDER BY ma_tin_tuc DESC LiMIT 5";
						$array = mysqli_query($con, $sql);
						?>
						<?php
						while ($row_news = mysqli_fetch_array($array)) {
						?>
							<div class="content-news-box">
								<img src="admin/modules/blog/uploads_tt/<?php echo $row_news['anh'] ?>" width="60px" height="60px">
								<a href="blog_detail.php?matt=<?php echo $row_news['ma_tin_tuc'] ?>">
									<h3><?php echo $row_news['tieu_de'] ?></h3>
								</a>
								<span>
									<?php
									$date = $row_news['ngay_dang_tin'];
									$timestamp = strtotime($date);
									$new_date = date("d/m/Y", $timestamp);
									echo $new_date;
									?>
								</span>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
				<div class="list-view-cart">
					<div class="healing-cart">
						<h2>Giỏ hàng của bạn</h2>
					</div>
					<table>
						<tr>
							<th>STT</th>
							<th>Tên sản phẩm</th>
							<th>Ảnh sản phẩm</th>
							<th>Giá tiền</th>
							<th>Số lượng</th>
							<th>Tổng tiền</th>
							<th class="line">Thao tác</th>
						</tr>
						<?php
						$i = 0;
						$tong_tien = 0;
						foreach ($arr_gio_hang as $ma_san_pham => $each) : $i++ ?>
							<?php
							$thanh_tien = ($each['price'] * $each['num']);
							$tong_tien += $thanh_tien;
							?>
							<?php

							$sql = "SELECT * FROM san_pham WHERE ma_san_pham = '$ma_san_pham '";
							$result = mysqli_query($con, $sql);
							$row = mysqli_fetch_array($result);
							$sales = "-" . ($row['sales'] * 100) . "%";
							?>
							<tr>
								<td><?php echo $i ?></td>
								<td>
									<a href="?m=view_product_detail&ma=<?php echo $each['id'] ?>"><?php echo $each['name'] ?></a>
									<strong style="display: block; font-weight: normal; font-size: 15px; margin-top:7px">Cỡ giày: <?php echo $each['size'] ?></strong>
								</td>
								<td>
									<img src="admin/modules/product_details/uploads_product/<?php echo $each['image'] ?>" width="60px" height="60px" />
								</td>
								<td style="font-weight: bold;">
									<?php echo number_format($each['price'], 0, ",", ".") ?> <span>₫</span>
									<strong style="display: block; font-weight: normal;"><del><?php echo number_format($row['gia'], 0, ",", ".") ?> <span>₫</span></del><i style="color: #fff;margin-left: 4px; font-size: 10px; vertical-align: top; background: red; border-radius: 35%; padding: 1px"><?php echo $sales ?></i></strong>
								</td>
								<td>
									<a class="update_cart sub" href="update_num_cart.php?ma=<?php echo $each['id'] ?>&kieu=tru" style="border:1px solid darkgray; color:#000">
										-
									</a>
									<strong style="padding: 0px 5px 1px 5px; border: 1px solid darkgray; font-size: 14px;"><?php echo $each['num'] ?></strong><a class="update_cart" href="update_num_cart.php?ma=<?php echo $each['id'] ?>&kieu=cong" style="border:1px solid darkgray; color:#000">+</a>
								</td>
								<td>
									<?php echo number_format($thanh_tien, 0, ",", ".") ?> <span>₫</span>
								</td>
								<td>
									<a href="delete_product_cart.php?ma=<?php echo $each['id'] ?>" onclick="return confirm('Bạn có chắc chắn xóa không?');" style="border:none;">
										<img src="images/delete.png" width="25" height="25">
									</a>
								</td>
							</tr>
						<?php endforeach ?>
					</table>
					<div class="plus-cart">
						<div class="content-plus-cart">
							<h2>Cộng giỏ hàng</h2>
							<h3>Tổng cộng: <span id="tong_tien_tat_ca"><?php echo number_format($tong_tien, 0, ",", ".") ?> <span>₫</span></span></h3>
							<div class="buttom">
								<a href="checkout.php">Tiến hành thanh toán</a>
							</div>
						</div>
					</div>
				</div>
			<?php } else {
			echo '<h1>Giỏ hàng của bạn đang trống</h1>
					<p>Không có sản phẩm nào. Quay lại <a href="http://project.com/">cửa hàng</a> để tiếp tục mua sắm. </p>
					';
		} ?>
			</div>
	</div>
</div>
<div class="clear"></div>
