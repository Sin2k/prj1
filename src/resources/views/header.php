<header class="header">

	<div class="header-wrap">
		<div class="grid wide">
			<div class="header__top">
				<div class="header-hotline">
					<img src="src/public/images/icons/icon-hotline.gif" alt="#" class="header-hotline__img">
					<p class="header-hotline__text">HOTLINE: 0128803267</p>
				</div>

				<div class="header-address">
					<a href="?m=address_shop" class="header-address__link">
						<i class="fas fa-map-marked-alt header-address__icon"></i>
						Địa chỉ cửa hàng
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="grid wide">
		<div class="header__bottom">
			<div class="header-logo">
				<a href="/" class="header-logo__link">
					<img src="src/public/images/logo/shoesvn.jpg" alt="#" class="header-logo__img">
				</a>
			</div>

			<div class="header-with-search">
				<form class="header-search" action="search.php" method="GET">
					<div class="header-search__wrap">
						<input type="hidden" name="price" />
						<input type="text" class="header-search__text" name="search" value="<?php if (isset($_GET['search'])) { ?><?php echo $_GET['search'] ?><?php } else { ?><?php echo '' ?><?php } ?>" placeholder="Search product..." />
					</div>

					<button class="header-search__btn">
						<img src="src/public/images/search.png" class="header-search__img">
					</button>
				</form>
			</div>

			<div class="header-with-login">
				<div class="header-login__logo">
					<img src="src/public/images/user.png" alt="" class="header-login__img">
				</div>
				<?php
				if (empty($_SESSION['ma_khach_hang'])) {
				?>
					<ul class="header-login__option">
						<li class="header-login__option-item">
							<a href="?m=register" class="header-login__option-link">Đăng ký & Lưu</a>
						</li>
						<li class="header-login__option-item">
							<a href="?m=login" class="header-login__option-link">Đăng nhập</a>
						</li>
					</ul>
				<?php } else { ?>
					<ul class="header-login__option">
						<li class="header-login__option-item">
							<a href="?m=account" class="header-login__option-link">Tài khoản</a>
						</li>
					</ul>
				<?php } ?>
			</div>
		</div>
	</div>

	<div class="menu-wrap">
		<div class="grid wide">
			<div class="header__menu">
				<?php
					$get_brand = "select * from hang";
					$result = mysqli_query($con, $get_brand);
				?>

				<ul class="header__menu-list ">
					<li class="header__menu-item">
						<a href="/" class="header__menu-link">
							<span class="header__menu-span header__menu-span--color-page">
								Trang chủ
							</span>
						</a>
					</li>
					<li class="header__menu-item">
						<a href="?m=all_product" class="header__menu-link">
							<span class="header__menu-span header__menu-span--color-product">
								Tất cả sản phẩm
							</span>
						</a>
					</li>

					<?php
						while ($row_brand = mysqli_fetch_assoc($result)) {
					?>
						<li class="header__menu-item">
							<a href="?m=product&id=<?php echo $row_brand['ma_hang'] ?>" class="header__menu-link">
								<span class="header__menu-span header__menu-span--color<?php echo $row_brand['ma_hang']?>">
									<?php echo $row_brand['ten_hang'] ?>
								</span>
							</a>
						</li>
					<?php } ?>
					
					<li class="header__menu-item">
						<a href="?m=blog" class="header__menu-link">
							<span class="header__menu-span header__menu-span--color-new">
								Tin tức
							</span>
						</a>
					</li>
				</ul>

				<ul class="header__menu-cart">
					<?php 
						$numberCart = 0;
						if(isset($_SESSION["cart"])){
							foreach ($_SESSION["cart"] as $value) {
								$numberCart ++;
							}
						}
					?>	
							
					<li class="header__menu-cart-item header__menu-cart-item--separated">
						<span class="header__menu-cart-text">MY CART</span>
					</li>

					<li class="header__menu-cart-item">
						<a href="?m=view_gio_hang" class="header__menu-cart-link">
							<img src="src/public/images/cart-icon.png" alt="#" class="header__menu-cart-img">
						</a>
					</li>

					<div class="header-cart">
						<ul class="header-cart__list">
							<?php 
								if(isset($_SESSION["cart"]) && count($_SESSION['cart'])>0) {
									$arr_cart = $_SESSION["cart"];
							?>
							
							<h1>Sản phẩm đã đặt mua</h1>

							<?php 
								$tong_tien = 0;
								foreach ($arr_cart as $key => $each) : 
							?>

							<?php
								$thanh_tien = ($each['price'] * $each['num']);
								$tong_tien += $thanh_tien;
							?>

							<li class="header-cart__item">
								<div class="cart-swap">
									<img src="admin/modules/product_details/uploads_product/<?php echo $each['image'] ?>" class="cart-img">
									
									<div class="cart-info">
										<span>
											<?php echo $each['name'] ?>	
										</span>
										<p>
											<?php echo $each['num'] ?> x <?php echo number_format($each['price'],0,",",".") ?> ₫
										</p>
										<p>
											Kích cỡ - <?php echo $each['size'] ?>
										</p>
									</div>

									<div class="cart-action">
										<a href="?m=view_product_detail&ma=<?php echo $each['id'] ?>" class="cart-action__link">
											<img class="cart-action__img" src="src/public/images/pen1.png">
										</a>
										<a href="delete_product_cart_header.php?ma=<?php echo $each['id'] ?>" class="cart-action__link" 
											onclick="return confirm('Bạn có chắc chắn muốn xoá bỏ mục này từ các giỏ mua hàng?');">
											<img class="cart-action__img" src="src/public/images/delete1.png">
										</a>
									</div>
								</div>
							</li>

							<?php 
								endforeach 
							?>

							<li class="header-cart__item">
								<div class="header-cart__total">
									Total: <?php echo number_format($tong_tien,0,",",".") ?> ₫
								</div>
							</li>

							<div class="header-cart__wrap">
								<button type="button" class="btn-cart" onclick="window.location = '?m=view_gio_hang'">
									<span class="btn-cart--effect">
										<span class="btn-cart--effect-hover">Giỏ hàng</span>
									</span>
								</button>

								<button type="button" class="btn-pay" onclick="window.location = '?m=checkout'">
									<span class="btn-pay--effect">
										<span class="btn-pay--effect-hover">Thanh toán</span>
									</span>
								</button>
							</div>

							<?php 
								}else{
									echo '<li>
											<h1>Giỏ hàng của bạn đang trống</h1>
										</li>
									'; 
								}
							?>
						</ul>
					</div>
				</ul>
			</div>
		</div>
	</div>
</header>