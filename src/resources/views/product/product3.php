<?php
if (empty($_GET['category']) || empty($_GET['id']) || empty($_GET['type'])) {
	header('location:all_product.php');
	exit();
}
?>

<div class="main-all-product">
	<div class="wrap">
		<div class="breadcrumb iiii">
			<ul class="breadcrumbs">
				<li><a href="/">Trang chủ<i class="fas fa-home"></i></a></li>
				<?php if ($_GET['id'] == 1) { ?>
					<li>
						<?php if ($_GET['category'] == 1) { ?>
							<a href="product1.php?category=1">Nam</a>
						<?php } else { ?>
							<a href="product1.php?category=2">Nữ</a>
						<?php } ?>
					</li>
					<li><span>New Balance</span></li>
				<?php } else if ($_GET['id'] == 2) { ?>
					<li>
						<?php if ($_GET['category'] == 1) { ?>
							<a href="product1.php?category=1">Nam</a>
						<?php } else { ?>
							<a href="product1.php?category=2">Nữ</a>
						<?php } ?>
					</li>
					<li>
						<?php if ($_GET['category'] == 1 && $_GET['id'] == 2) { ?>
							<a href="?m=product2&category=1&id=2">Giày thể thao Nike</a>
						<?php } else { ?>
							<a href="?m=product2&category=2&id=2">Giày thể thao Nike</a>
						<?php } ?>
					</li>
					<li>
						<?php if ($_GET['type'] == 1) { ?>
							<span>Air Max</span>
						<?php } else if ($_GET['type'] == 2) { ?>
							<span>Air Force 1</span>
						<?php } else if ($_GET['type'] == 3) { ?>
							<span>Air Zoom</span>
						<?php } else if ($_GET['type'] == 4) { ?>
							<span>Air Jordan 1</span>
						<?php } else { ?>

						<?php } ?>
					</li>
				<?php } else if ($_GET['id'] == 3) { ?>
					<li>
						<?php if ($_GET['category'] == 1) { ?>
							<a href="product1.php?category=1">Nam</a>
						<?php } else { ?>
							<a href="product1.php?category=2">Nữ</a>
						<?php } ?>
					</li>
					<li>
						<?php if ($_GET['category'] == 1 && $_GET['id'] == 3) { ?>
							<a href="?m=product2&category=1&id=3">Giày thể thao Adidas</a>
						<?php } else { ?>
							<a href="?m=product2&category=2&id=3">Giày thể thao Adidas</a>
						<?php } ?>
					</li>
					<li>
						<?php if ($_GET['type'] == 5) { ?>
							<span>Ultra Boost</span>
						<?php } else if ($_GET['type'] == 6) { ?>
							<span>Alphabounce</span>
						<?php } else if ($_GET['type'] == 7) { ?>
							<span>Stan Smith</span>
						<?php } else if ($_GET['type'] == 8) { ?>
							<span>Yeezy</span>
						<?php } else { ?>
						<?php } ?>
					</li>
				<?php } else if ($_GET['id'] == 4) { ?>
					<li>
						<?php if ($_GET['category'] == 1) { ?>
							<a href="product1.php?category=1">Nam</a>
						<?php } else { ?>
							<a href="product1.php?category=2">Nữ</a>
						<?php } ?>
					</li>
					<li>
						<?php if ($_GET['category'] == 1 && $_GET['id'] == 4) { ?>
							<a href="?m=product2&category=1&id=4">Vans</a>
						<?php } else { ?>
							<a href="?m=product2&category=2&id=4">Vans</a>
						<?php } ?>
					</li>
					<li>
						<?php if ($_GET['type'] == 9) { ?>
							<span>Old Skool</span>
						<?php } else if ($_GET['type'] == 10) { ?>
							<span>SK8-Hi</span>
						<?php } else { ?>
						<?php } ?>
					</li>
				<?php } else if ($_GET['id'] == 5) { ?>
					<li>
						<?php if ($_GET['category'] == 1) { ?>
							<a href="product1.php?category=1">Nam</a>
						<?php } else { ?>
							<a href="product1.php?category=2">Nữ</a>
						<?php } ?>
					</li>
					<li>
						<?php if ($_GET['category'] == 1 && $_GET['id'] == 5) { ?>
							<a href="?m=product2&category=1&id=5">Puma</a>
						<?php } else { ?>
							<a href="?m=product2&category=2&id=5">Puma</a>
						<?php } ?>
					</li>
					<li>
						<?php if ($_GET['type'] == 11) { ?>
							<span>Smash</span>
						<?php } else if ($_GET['type'] == 12) { ?>
							<span>Muse X</span>
						<?php } else { ?>
						<?php } ?>
					</li>
				<?php } else if ($_GET['id'] == 6) { ?>
					<li>
						<?php if ($_GET['category'] == 1) { ?>
							<a href="product1.php?category=1">Nam</a>
						<?php } else { ?>
							<a href="product1.php?category=2">Nữ</a>
						<?php } ?>
					</li>
					<li>
						<?php if ($_GET['category'] == 1 && $_GET['id'] == 6) { ?>
							<a href="?m=product2&category=1&id=6">Converse</a>
						<?php } else { ?>
							<a href="?m=product2&category=2&id=6">Converse</a>
						<?php } ?>
					</li>
					<li>
						<?php if ($_GET['type'] == 13) { ?>
							<span>Jack Purcell</span>
						<?php } else if ($_GET['type'] == 14) { ?>
							<span>Chuck Taylor</span>
						<?php } else { ?>
						<?php } ?>
					</li>
				<?php } else { ?>

				<?php } ?>
			</ul>
		</div>
		<div style="width: 97%; margin: auto;">
			<div class="side-bar">
				<?php
				$sql_danhmuc = "select * from danh_muc";
				$result = mysqli_query($con, $sql_danhmuc);
				?>
				<div class="category">
					<div class="healing-category">
						<div class="healing-category-img">
							<img src="src/public/images/menu.png" width="30px" height="30px">
						</div>
						<div class="healing-category-txt">
							<p>
								Danh mục sản phẩm
							</p>
						</div>
					</div>
					<ul id="menutree">
						<?php
						while ($row_danhmuc = mysqli_fetch_array($result)) {
						?>
							<?php if ($row_danhmuc['ma_danh_muc'] == 1) { ?>
								<li class="collapse">
									<input type="checkbox" id="m0" />
									<label for="m0">
										<a href="?m=product1&category=1"><?php echo $row_danhmuc['ten_danh_muc'] ?></a>
									</label>
									<ul>
										<li class="collapse">
											<input type="checkbox" id="m1" />
											<label for="m1">
												<a href="?m=product2&category=1&id=3">Adidas</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=1&id=3&type=5">Ultra Boost</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=3&type=6">Alphabounce</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=3&type=7">Stan Smith</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=3&type=8">Yeezy</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m2" />
											<label for="m2">
												<a href="?m=product2&category=1&id=2">Nike</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=1&id=2&type=1">Air Max</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=2&type=2">Air Force 1</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=2&type=3">Air Zoom</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=2&type=4">Air Jordan 1</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m3" />
											<label for="m3">
												<a href="?m=product2&category=1&id=4">Vans</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=1&id=4&type=9">Old Skool</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=4&type=10">SK8-Hi</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m4" />
											<label for="m4">
												<a href="?m=product2&category=1&id=5">Puma</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=1&id=5&type=11">Smash</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=5&type=12">Muse X</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li>
											<a href="?m=product2&category=1&id=1">New Balance</a>
											<span class="arw"></span>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m5" />
											<label for="m5">
												<a href="?m=product2&category=1&id=6">Converse</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=1&id=6&type=13">Jack Purcell</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=6&type=14">Chuck Taylor</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
									</ul>
								</li>
							<?php } else if ($row_danhmuc['ma_danh_muc'] == 2) { ?>
								<li class="collapse">
									<input type="checkbox" id="m6" />
									<label for="m6">
										<a href="?m=product1&category=2"><?php echo $row_danhmuc['ten_danh_muc'] ?></a>
									</label>
									<ul>
										<li class="collapse">
											<input type="checkbox" id="m7" />
											<label for="m7">
												<a href="?m=product2&category=2&id=3">Adidas</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=2&id=3&type=5">Ultra Boost</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=3&type=6">Alphabounce</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=3&type=7">Stan Smith</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=3&type=8">Yeezy</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m8" />
											<label for="m8">
												<a href="?m=product2&category=2&id=2">Nike</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=2&id=2&type=1">Air Max</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=2&type=2">Air Force 1</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=2&type=3">Air Zoom</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=2&type=4">Air Jordan 1</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m9" />
											<label for="m9">
												<a href="?m=product2&category=2&id=4">Vans</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=2&id=4&type=9">Old Skool</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=4&type=10">SK8-Hi</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m10" />
											<label for="m10">
												<a href="?m=product2&category=2&id=5">Puma</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=2&id=5&type=11">Smash</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=5&type=12">Muse X</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li>
											<a href="?m=product2&category=2&id=1">New Balance</a>
											<span class="arw"></span>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m11" />
											<label for="m11">
												<a href="?m=product2&category=2&id=6">Converse</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=2&id=6&type=13">Jack Purcell</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=6&type=14">Chuck Taylor</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
									</ul>
								</li>
							<?php } else { ?>
						<?php }
						} ?>
					</ul>
				</div>
				<div class="search-price-product">
					<div class="title-price">
						<p>Giá tiền</p>
					</div>
					<form class="form-price" action="search.php" method="get" onsubmit="return checkPrice()">
						<input type="hidden" name="search" />
						<select name="price" id="select_price">
							<option value="0"> Giá sản phẩm </option>
							<option value="Dưới 1.000.000₫">Dưới 1.000.000 ₫</option>
							<option value="1.000.000₫ - 3.000.000₫">1.000.000 ₫ - 3.000.000 ₫</option>
							<option value="3.000.000₫ - 5.000.000₫">3.000.000 ₫ - 5.000.000 ₫</option>
							<option value="5.000.000₫ - 8.000.000₫">5.000.000 ₫ - 8.000.000 ₫</option>
							<option value="8.000.000₫ - 10.000.000₫">8.000.000 ₫ - 10.000.000 ₫</option>
							<option value="Trên 10.000.000₫">Trên 10.000.000 ₫</option>
						</select>
						<span class="fix-price" id="err_select_price"></span>
						<button type="submit" class="btn-tk">Tìm kiếm</button>
					</form>
				</div>
				<div class="news-product">
					<div class="title-news">
						<p>Bài viết mới nhất</p>
						<span></span>
					</div>
					<div class="content-news">
						<?php
						$sql = "SELECT * FROM tin_tuc ORDER BY ma_tin_tuc DESC LiMIT 5";
						$array = mysqli_query($con, $sql);
						?>
						<?php
						while ($row_news = mysqli_fetch_array($array)) {
						?>
							<div class="content-news-box">
								<img src="admin/modules/blog/uploads_tt/<?php echo $row_news['anh'] ?>" width="60px" height="60px">
								<a href="blog_detail.php?matt=<?php echo $row_news['ma_tin_tuc'] ?>">
									<h3><?php echo $row_news['tieu_de'] ?></h3>
								</a>
								<span>
									<?php
									$date = $row_news['ngay_dang_tin'];
									$timestamp = strtotime($date);
									$new_date = date("d/m/Y", $timestamp);
									echo $new_date;
									?>
								</span>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="list-all-product">
				<?php
				$sql = "select * from san_pham where ma_danh_muc='$_GET[category]' AND ma_hang='$_GET[id]' AND ma_loai_san_pham='$_GET[type]'";
				$array = mysqli_query($con, $sql);
				$tong_so_san_pham = mysqli_num_rows($array);
				$limit = !empty($_GET['limit']) ? $_GET['limit'] : 6;
				$trang_hien_tai = !empty($_GET['page']) ? $_GET['page'] : 1;
				if (isset($_GET['page'])) {
					$trang_hien_tai = $_GET['page'];
				}
				$trang_ke_tiep = ($trang_hien_tai - 1) * $limit;
				$so_trang = ceil($tong_so_san_pham / $limit);
				$sql = "$sql limit $limit offset $trang_ke_tiep";
				$array = mysqli_query($con, $sql);
				?>
				<div class="advertisement">
					<?php if ($_GET['category'] == 1) { ?>
						<?php if ($_GET['id'] == 2 && $_GET['type'] == 1) { ?>
							<h1>
								Air Max
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Air Max</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/Air_Max_nam.jpg" width="440px" height="295px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 2 && $_GET['type'] == 2) { ?>
							<h1>
								Air Force 1
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Air Force 1</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/Air_Force_1_nam.jpg" width="440px" height="295px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 2 && $_GET['type'] == 3) { ?>
							<h1>
								Air Zoom
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Air Zoom</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 2 && $_GET['type'] == 4) { ?>
							<h1>
								Air Jordan 1
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Air Jordan 1</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 3 && $_GET['type'] == 5) { ?>
							<h1>
								Ultra Boost
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Ultra Boost</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 3 && $_GET['type'] == 6) { ?>
							<h1>
								Alphabounce
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Alphabounce</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 3 && $_GET['type'] == 7) { ?>
							<h1>
								Stan Smith
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Stan Smith</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 3 && $_GET['type'] == 8) { ?>
							<h1>
								Yeezy
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Yeezy</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 4 && $_GET['type'] == 9) { ?>
							<h1>
								Old Skool
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Old Skool</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 4 && $_GET['type'] == 10) { ?>
							<h1>
								SK8-Hi
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>SK8-Hi</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 5 && $_GET['type'] == 11) { ?>
							<h1>
								Smash
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Smash</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 5 && $_GET['type'] == 12) { ?>
							<h1>
								Muse X
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Muse X</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 6 && $_GET['type'] == 13) { ?>
							<h1>
								Jack Purcell
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Jack Purcell</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 6 && $_GET['type'] == 14) { ?>
							<h1>
								Chuck Taylor
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Chuck Taylor</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else { ?>
							<?php echo '<div class="infoss">
									<p> 404 Not Found! </p>
								</div>';
							?>
						<?php } ?>
					<?php } else if ($_GET['category'] == 2) { ?>
						<?php if ($_GET['id'] == 2 && $_GET['type'] == 1) { ?>
							<h1>
								Air Max
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Air Max</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/Air_Max_nu.jpg" width="440px" height="295px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 2 && $_GET['type'] == 2) { ?>
							<h1>
								Air Force 1
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Air Force 1</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/Air_Force_1_nu.jpg" width="440px" height="295px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 2 && $_GET['type'] == 3) { ?>
							<h1>
								Air Zoom
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Air Zoom</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 2 && $_GET['type'] == 4) { ?>
							<h1>
								Air Jordan 1
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Air Jordan 1</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 3 && $_GET['type'] == 5) { ?>
							<h1>
								Ultra Boost
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Ultra Boost</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 3 && $_GET['type'] == 6) { ?>
							<h1>
								Alphabounce
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Alphabounce</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 3 && $_GET['type'] == 7) { ?>
							<h1>
								Stan Smith
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Stan Smith</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 3 && $_GET['type'] == 8) { ?>
							<h1>
								Yeezy
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Yeezy</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 4 && $_GET['type'] == 9) { ?>
							<h1>
								Old Skool
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Old Skool</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 4 && $_GET['type'] == 10) { ?>
							<h1>
								SK8-Hi
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>SK8-Hi</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 5 && $_GET['type'] == 11) { ?>
							<h1>
								Smash
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Smash</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 5 && $_GET['type'] == 12) { ?>
							<h1>
								Muse X
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Muse X</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 6 && $_GET['type'] == 13) { ?>
							<h1>
								Jack Purcell
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Jack Purcell</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else if ($_GET['id'] == 6 && $_GET['type'] == 14) { ?>
							<h1>
								Chuck Taylor
							</h1>
							<div class="description">
								<p>Hãy lựa chọn những đôi giày thể thao <strong>Chuck Taylor</strong> phù hợp với phong cách của bạn.</p>
							</div>
							<div class="images mn">
								<img src="src/public/images/logo/new_balance_logo.jpg" width="290px" height="290px" style="padding: 0px 0 32px 0 !important;">
							</div>
						<?php } else { ?>
							<?php echo '<div class="infoss">
										<p> 404 Not Found! </p>
									</div>';
							?>
						<?php } ?>
					<?php } else { ?>
						<?php echo '<div class="infoss">
									<p> 404 Not Found! </p>
								</div>';
						?>
					<?php } ?>
				</div>
				<div class="toolbar" id="myDIV">
					<div class="pagination">
						<?php for ($i = 1; $i <= $so_trang; $i++) { ?>
							<?php if ($i != $trang_hien_tai) { ?>
								<a href="?category=<?php echo $_GET['category'] ?>&id=<?php echo $_GET['id'] ?>&type=<?php echo $_GET['type'] ?>&page=<?php echo $i ?>"><button class="so active"><?php echo $i; ?></button></a>
							<?php } else { ?>
								<strong class="so" style="background: #333; color: #fff"><?= $i ?></strong>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
				<div class="show-all-product">
					<?php
					while ($row_cat_bran_type_product = mysqli_fetch_array($array)) {
						$gia_ban = ($row_cat_bran_type_product['gia'] - ($row_cat_bran_type_product['gia'] * $row_cat_bran_type_product['sales']));
						$gia_fm = number_format($row_cat_bran_type_product['gia'], "0", ",", ".") . " " . "₫";
						$gia_banfm = number_format($gia_ban, "0", ",", ".") . " " . "₫";
						$sales = ($row_cat_bran_type_product['sales'] * 100) . "%";
					?>
						<div class="box-bottom box-content-bottom">
							<a href="">
								<div class="view view-fifth">
									<div class="top_box">
										<h3 class="text_3"><?php echo $row_cat_bran_type_product['ten_san_pham'] ?></h3>
										<p class="text_2">Lorem ipsum</p>
										<div class="grid_img">
											<div class="imgs">
												<img src="admin/modules/product_details/uploads_product/<?php echo $row_cat_bran_type_product['anh'] ?>" width="170px" height="170px" alt="" />
											</div>
											<a href="?m=view_product_detail&ma=<?php echo $row_cat_bran_type_product['ma_san_pham'] ?>">
												<div class="mask1">
													<div class="info1">
														Quick View
													</div>
												</div>
											</a>
										</div>

										<?php
										if ($row_cat_bran_type_product['sales'] > 0) {
											echo "<h6>SALE  $sales</h6>";
										} else {
											echo "";
										}
										?>

										<?php if ($row_cat_bran_type_product['tinh_trang'] == 0) { ?>
											<p style="display: inline-block; border: 1px solid #000; position: absolute;transform: rotate(90deg);right: -52px; top: 132px; padding-top: 1px "><img src="src/public/images/cai_no.jpg" style="width: 25px;height: 20px;vertical-align: middle;transform: rotate(92deg);">Tạm hết hàng<img src="src/public/images/cai_no.jpg" style="width: 25px;height: 20px;vertical-align: middle;transform: rotate(-87deg);">
											</p>
										<?php } else { ?>

										<?php } ?>
									</div>
								</div>

								<div class="price">
									<?php
									if ($row_cat_bran_type_product['sales'] > 0) {
										echo "<span>$gia_fm</span>";
										echo "$gia_banfm";
									} else {
										echo "<strong>$gia_fm</strong>";
									}
									?>
								</div>

								<ul class="list-product">
									<li>
										<img class="imgs-product" src="src/public/images/plus.png" alt="" />
										<ul class="icon1 sub-icon1 profile_img">
											<li>
												<?php if (empty($_SESSION['ma_khach_hang'])) { ?>
													<a class="active-icon c1" href="?m=login" onclick="return confirm('Bạn cần có tài khoản thì mới được đặt mua sản phẩm');">Add To Bag </a>
												<?php } else { ?>
													<?php if ($row_cat_bran_type_product['tinh_trang'] == 1) { ?>
														<a class="active-icon c1" href="add_cart1.php?ma=<?php echo $row_cat_bran_type_product['ma_san_pham'] ?>" onclick="return alert('Bạn đã thêm sản phẩm vào giỏ hàng');">Add To Bag </a>
													<?php } else { ?>
														<a class="active-icon c1" style="cursor: pointer;" onclick="return alert('Sản phẩm tạm hết hàng');">Add To Bag </a>
													<?php } ?>
												<?php } ?>
											</li>
										</ul>
									</li>
								</ul>
								<div class="clear"></div>
							</a>
						</div>
					<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
