<div class="main-all-product">
	<div class="wrap">
		<div class="breadcrumb iiii">
			<ul class="breadcrumbs">
				<li><a href="/">Trang chủ<i class="fas fa-home"></i></a></li>
				<li><span>Tất cả giày thể thao</span></li>
			</ul>
		</div>
		<div style="width: 97%; margin: auto;">
			<div class="side-bar">
				<?php
				$sql_danhmuc = "select * from danh_muc";
				$result = mysqli_query($con, $sql_danhmuc);
				?>
				<div class="category">
					<div class="healing-category">
						<div class="healing-category-img">
							<img src="src/public/images/menu.png" width="30px" height="30px">
						</div>
						<div class="healing-category-txt">
							<p>
								Danh mục sản phẩm
							</p>
						</div>
					</div>
					<ul id="menutree">
						<?php
						while ($row_danhmuc = mysqli_fetch_array($result)) {
						?>
							<?php if ($row_danhmuc['ma_danh_muc'] == 1) { ?>
								<li class="collapse">
									<input type="checkbox" id="m0" />
									<label for="m0">
										<a href="?m=product1&category=1"><?php echo $row_danhmuc['ten_danh_muc'] ?></a>
									</label>
									<ul>
										<li class="collapse">
											<input type="checkbox" id="m1" />
											<label for="m1">
												<a href="?m=product2&category=1&id=3">Adidas</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=1&id=3&type=5">Ultra Boost</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=3&type=6">Alphabounce</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=3&type=7">Stan Smith</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=3&type=8">Yeezy</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m2" />
											<label for="m2">
												<a href="?m=product2&category=1&id=2">Nike</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=1&id=2&type=1">Air Max</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=2&type=2">Air Force 1</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=2&type=3">Air Zoom</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=2&type=4">Air Jordan 1</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m3" />
											<label for="m3">
												<a href="?m=product2&category=1&id=4">Vans</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=1&id=4&type=9">Old Skool</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=4&type=10">SK8-Hi</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m4" />
											<label for="m4">
												<a href="?m=product2&category=1&id=5">Puma</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=1&id=5&type=11">Smash</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=5&type=12">Muse X</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li>
											<a href="?m=product2&category=1&id=1">New Balance</a>
											<span class="arw"></span>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m5" />
											<label for="m5">
												<a href="?m=product2&category=1&id=6">Converse</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=1&id=6&type=13">Jack Purcell</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=1&id=6&type=14">Chuck Taylor</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
									</ul>
								</li>
							<?php } else if ($row_danhmuc['ma_danh_muc'] == 2) { ?>
								<li class="collapse">
									<input type="checkbox" id="m6" />
									<label for="m6">
										<a href="?m=product1&category=2"><?php echo $row_danhmuc['ten_danh_muc'] ?></a>
									</label>
									<ul>
										<li class="collapse">
											<input type="checkbox" id="m7" />
											<label for="m7">
												<a href="?m=product2&category=2&id=3">Adidas</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=2&id=3&type=5">Ultra Boost</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=3&type=6">Alphabounce</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=3&type=7">Stan Smith</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=3&type=8">Yeezy</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m8" />
											<label for="m8">
												<a href="?m=product2&category=2&id=2">Nike</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=2&id=2&type=1">Air Max</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=2&type=2">Air Force 1</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=2&type=3">Air Zoom</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=2&type=4">Air Jordan 1</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m9" />
											<label for="m9">
												<a href="?m=product2&category=2&id=4">Vans</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=2&id=4&type=9">Old Skool</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=4&type=10">SK8-Hi</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m10" />
											<label for="m10">
												<a href="?m=product2&category=2&id=5">Puma</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=2&id=5&type=11">Smash</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=5&type=12">Muse X</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
										<li>
											<a href="?m=product2&category=2&id=1">New Balance</a>
											<span class="arw"></span>
										</li>
										<li class="collapse">
											<input type="checkbox" id="m11" />
											<label for="m11">
												<a href="?m=product2&category=2&id=6">Converse</a>
												<span></span>
											</label>
											<ul>
												<li>
													<a href="?m=product3&category=2&id=6&type=13">Jack Purcell</a>
													<span class="childs"></span>
												</li>
												<li>
													<a href="?m=product3&category=2&id=6&type=14">Chuck Taylor</a>
													<span class="childs"></span>
												</li>
											</ul>
										</li>
									</ul>
								</li>
							<?php } else { ?>
						<?php }
						} ?>
					</ul>
				</div>
				<div class="search-price-product">
					<div class="title-price">
						<p>Giá tiền</p>
					</div>
					<form class="form-price" action="search.php" method="get" onsubmit="return checkPrice()">
						<input type="hidden" name="search" />
						<select name="price" id="select_price">
							<option value="0"> Giá sản phẩm </option>
							<option value="Dưới 1.000.000₫">Dưới 1.000.000 ₫</option>
							<option value="1.000.000₫ - 3.000.000₫">1.000.000 ₫ - 3.000.000 ₫</option>
							<option value="3.000.000₫ - 5.000.000₫">3.000.000 ₫ - 5.000.000 ₫</option>
							<option value="5.000.000₫ - 8.000.000₫">5.000.000 ₫ - 8.000.000 ₫</option>
							<option value="8.000.000₫ - 10.000.000₫">8.000.000 ₫ - 10.000.000 ₫</option>
							<option value="Trên 10.000.000₫">Trên 10.000.000 ₫</option>
						</select>
						<span class="fix-price" id="err_select_price"></span>
						<button type="submit" class="btn-tk">Tìm kiếm</button>
					</form>
				</div>
				<div class="news-product">
					<div class="title-news">
						<p>Bài viết mới nhất</p>
						<span></span>
					</div>
					<div class="content-news">
						<?php
						$sql = "SELECT * FROM tin_tuc ORDER BY ma_tin_tuc DESC LiMIT 5";
						$array = mysqli_query($con, $sql);
						?>
						<?php
						while ($row_news = mysqli_fetch_array($array)) {
						?>
							<div class="content-news-box">
								<img src="admin/modules/blog/uploads_tt/<?php echo $row_news['anh'] ?>" width="60px" height="60px">
								<a href="blog_detail.php?matt=<?php echo $row_news['ma_tin_tuc'] ?>">
									<h3><?php echo $row_news['tieu_de'] ?></h3>
								</a>
								<span>
									<?php
									$date = $row_news['ngay_dang_tin'];
									$timestamp = strtotime($date);
									$new_date = date("d/m/Y", $timestamp);
									echo $new_date;
									?>
								</span>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="list-all-product">
				<div class="advertisement">
					<h1>
						Tất cả giày thể thao
					</h1>
					<div class="description">
						<p style="padding: 119px 73px">Tất cả sản phẩm giày chúng tôi</p>
					</div>
					<div class="images logos">
						<img src="src/public/images/logo/adidas_logo.png" width="55px" height="55px" style="top: 56px; left: 80px;">
						<img src="src/public/images/logo/nike_logo.jpg" width="75px" height="50px" style="top: 61px; left: 187px;">
						<img src="src/public/images/logo/vans_logo_small.png" width="65px" height="50px" style="top: 57px; right: 77px;">
						<img src="src/public/images/logo/puma_logo.png" width="65px" height="65px" style="bottom: 60px; left: 79px;">
						<img src="src/public/images/logo/new_balance_logo.jpg" width="80px" height="80px" style="bottom: 47px; left: 176px;">
						<img src="src/public/images/logo/converse_logo.jpg" width="80px" height="80px" style="bottom: 46px; right: 69px;">
					</div>
				</div>
				<?php
				$sql = "select * from san_pham";
				$array = mysqli_query($con, $sql);
				$tong_so_san_pham = mysqli_num_rows($array);
				$limit = !empty($_GET['limit']) ? $_GET['limit'] : 6;
				$trang_hien_tai = !empty($_GET['page']) ? $_GET['page'] : 1;
				if (isset($_GET['page']) && ($_GET['limit'])) {
					$trang_hien_tai = $_GET['page'];
					$limit = $_GET['limit'];
				}
				$trang_ke_tiep = ($trang_hien_tai - 1) * $limit; // Tính OFFSET
				$so_trang = ceil($tong_so_san_pham / $limit);
				$sql = "$sql limit $limit offset $trang_ke_tiep";
				$array = mysqli_query($con, $sql);
				?>
				<div class="toolbar" id="myDIV">
					<div class="pagination">
						<?php
						if ($trang_hien_tai > 3) {
							$first_page = 1;
						?>
							<a href="?limit=<?= $limit ?>&page=<?= $first_page ?>"><button class="so active">First</button></a>
						<?php
						}
						if ($trang_hien_tai > 1) {
							$prev_page = $trang_hien_tai - 1;
						?>
							<a href="?limit=<?= $limit ?>&page=<?= $prev_page ?>"><button class="so active">Prev</button></a>
						<?php } ?>

						<?php for ($i = 1; $i <= $so_trang; $i++) { ?>
							<?php if ($i != $trang_hien_tai) { ?>
								<?php if ($i > $trang_hien_tai - 3 && $i < $trang_hien_tai + 3) { ?>
									<a href="?limit=<?= $limit ?>&page=<?= $i ?>"><button class="so active"><?php echo $i; ?></button></a>
								<?php } ?>
							<?php } else { ?>
								<strong class="so" style="background: #333; color: #fff"><?= $i ?></strong>
							<?php } ?>
						<?php } ?>

						<?php
						if ($trang_hien_tai < $so_trang - 1) {
							$next_page = $trang_hien_tai + 1;
						?>
							<a href="?limit=<?= $limit ?>&page=<?= $next_page ?>"><button class="so active">Next</button></a>
						<?php
						}
						if ($trang_hien_tai < $so_trang - 3) {
							$end_page = $so_trang;
						?>
							<a href="?limit=<?= $limit ?>&page=<?= $end_page ?>"><button class="so active">Last</button></a>
						<?php
						}
						?>
					</div>
				</div>
				<div class="show-all-product">
					<?php
					while ($row_all_product = mysqli_fetch_array($array)) {
						$gia_ban = ($row_all_product['gia'] - ($row_all_product['gia'] * $row_all_product['sales']));
						$gia_fm = number_format($row_all_product['gia'], "0", ",", ".") . " " . "₫";
						$gia_banfm = number_format($gia_ban, "0", ",", ".") . " " . "₫";
						$sales = ($row_all_product['sales'] * 100) . "%";
					?>
						<div class="box-bottom box-content-bottom">
							<a href="">
								<div class="view view-fifth">
									<div class="top_box">
										<h3 class="text_3"><?php echo $row_all_product['ten_san_pham'] ?></h3>
										<p class="text_2">Lorem ipsum</p>
										<div class="grid_img">
											<div class="imgs">
												<img src="admin/modules/product_details/uploads_product/<?php echo $row_all_product['anh'] ?>" width="170px" height="170px" alt="File lỗi" />
											</div>
											<a href="?m=view_product_detail&ma=<?php echo $row_all_product['ma_san_pham'] ?>">
												<div class="mask1">
													<div class="info1">
														Quick View
													</div>
												</div>
											</a>
										</div>

										<?php
										if ($row_all_product['sales'] > 0) {
											echo "<h6>SALE  $sales</h6>";
										} else {
											echo "";
										}
										?>

										<?php if ($row_all_product['tinh_trang'] == 0) { ?>
											<p style="display: inline-block; border: 1px solid #000; position: absolute;transform: rotate(90deg);right: -52px; top: 132px; padding-top: 1px "><img src="src/public/images/cai_no.jpg" style="width: 25px;height: 20px;vertical-align: middle;transform: rotate(92deg);">Tạm hết hàng<img src="src/public/images/cai_no.jpg" style="width: 25px;height: 20px;vertical-align: middle;transform: rotate(-87deg);">
											</p>
										<?php } else { ?>

										<?php } ?>
									</div>
								</div>

								<div class="price">
									<?php
									if ($row_all_product['sales'] > 0) {
										echo "<span>$gia_fm</span>";
										echo "$gia_banfm";
									} else {
										echo "<strong>$gia_fm</strong>";
									}
									?>
								</div>

								<ul class="list-product">
									<li>
										<img class="imgs-product" src="src/public/images/plus.png" alt="" />
										<ul class="icon1 sub-icon1 profile_img">
											<li>
												<?php if ($row_all_product['tinh_trang'] == 1) { ?>
													<?php if (empty($_SESSION['ma_khach_hang'])) { ?>
														<a class="active-icon c1" href="?m=login" onclick="return confirm('Bạn cần có tài khoản thì mới được đặt mua sản phẩm');">Add To Bag </a>
													<?php } else { ?>
														<a class="active-icon c1" href="add_cart1.php?ma=<?php echo $row_all_product["ma_san_pham"] ?>" onclick="return alert('Bạn đã thêm sản phẩm vào giỏ hàng');">Add To Bag </a>
													<?php } ?>
												<?php } else { ?>
													<a class="active-icon c1" style="cursor: pointer;" onclick="return alert('Sản phẩm tạm hết hàng');">Add To Bag </a>
												<?php } ?>
											</li>
										</ul>
									</li>
								</ul>
								<div class="clear"></div>
							</a>
						</div>
					<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="clear"></div>
