<?php 
  if(!isset($_SESSION["ma_khach_hang"])){
      header("location:login.php");
      exit();
  }    
?>
<div class="main_bg_account">
<div class="wrap">
<div class="main-account">
<div class="account">
    <nav class="vertical-menu">
        <ul>
          <li class="dashboard">
            <i class="fas fa-tachometer-alt"></i><span>Bảng điều khiển</span>
          </li>
          <li class="general-information">
            <a href="?m=account">Thông tin chung</a>
          </li>
          <li class="account-information">
            <a href="?m=update_account">Thông tin tài khoản</a>
          </li>
          <li class="orders">
            <a href="?m=orders&khachhang=<?php echo $_SESSION['ma_khach_hang'] ?>">Đơn hàng</a>
          </li>
          <li class="recently-view active">
            <a href="?m=recently_view">Đã xem gần đây</a>
          </li>
          <li class="logout">
            <a href="../../../../logout.php">Đăng xuất</a>
          </li>
        </ul>
    </nav>
    <div class="account-content">
        <div class="title">
            <h1>Đã xem gần đây</h1>
        </div>       
    </div>
</div>
</div>
</div>
</div>


