<div class="content-bottom unm">
	<div class="wrap">
		<div class="tao-trang-chi dil">
			<div class="phan-trang-chi">
				<div class="trang-chi-img del">
					<img src="src/public/images/icons/policy-icon5.png" width="40px" height="40px" />
				</div>
				<div class="trang-chi-text">
					<h3>THANH TOÁN NHANH GỌN</h3>
					<p>Nhận thanh toán trực tiếp hoặc chuyển khoản</p>
				</div>
			</div>
		</div>
		<div class="tao-trang-chi">
			<div class="phan-trang-chi">
				<div class="trang-chi-img">
					<img src="src/public/images/icons/policy-icon4-32x32.png" width="40px" height="40px" />
				</div>
				<div class="trang-chi-text">
					<h3>NHIỀU ƯU ĐÃI HẤP DẪN</h3>
					<p>Thường nhận được ưu đãi và giảm giá</p>
				</div>
			</div>
		</div>
		<div class="tao-trang-chi">
			<div class="phan-trang-chi">
				<div class="trang-chi-img">
					<img src="src/public/images/icons/policy-icon3.png" width="40px" height="40px" />
				</div>
				<div class="trang-chi-text">
					<h3>ĐỔI SIZE MIỄN PHÍ</h3>
					<p>Đổi size trong thời gian 5 ngày</p>
				</div>
			</div>
		</div>
		<div class="tao-trang-chi">
			<div class="phan-trang-chi">
				<div class="trang-chi-img">
					<img src="src/public/images/icons/policy-icon2.png" width="40px" height="40px" />
				</div>
				<div class="trang-chi-text">
					<h3>HỖ TRỢ 24/12</h3>
					<p>Hỗ trợ trực tuyến 24h/ngày</p>
				</div>
			</div>
		</div>
		<div class="tao-trang-chi">
			<div class="phan-trang-chi">
				<div class="trang-chi-img">
					<img src="src/public/images/icons/policy-icon1.png" width="40px" height="40px" />
				</div>
				<div class="trang-chi-text">
					<h3>GIAO HÀNG TOÀN QUỐC</h3>
					<p>Nhận giao hàng tận nơi</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="footer">
	<div class="wrap-footer">
		<div class="footer-text">
			<span>Liên hệ</span>
			<ul>
				<li><i class="fas fa-map-marker-alt"></i> Thanh Xuân - Hà Nội</li>
				<li><i class="fas fa-phone-square-alt"></i> 01632618488</li>
				<li><i class="fas fa-envelope"></i>
					<a href="mailto:kelvinanh69@gmail.com?subject=Feedback&body=Message">kelvinanh69@gmail.com</a>
				</li>
			</ul>
		</div>
		<div class="footer-text">
			<span>F.A.Q</span>
			<ul>
				<li>
					<a class="middle" title="Mua hàng & thanh toán" href="?m=purchasing">Mua hàng & thanh toán</a>
				</li>
				<li>
					<a class="middle" title="Vận chuyển & giao hàng" href="?m=shipping">Vận chuyển & giao hàng</a>
				</li>
				<li>
					<a class="middle" title="Chính sách bảo mật" href="?m=privacing">Chính sách bảo mật</a>
				</li>
				<li>
					<a class="middle" title="Bảo hành và đổi trả" href="?m=warranting">Bảo hành và đổi trả</a>
				</li>
			</ul>
		</div>
		<div class="footer-text">
			<span>Theo dõi</span>
			<ul>
				<li>
					<a class="middle" title="Facebook" href="https://www.facebook.com/profile.php?id=100014658631318">Facebook</a>
				</li>
				<li>
					<a class="middle" title="Instagram" href="https://www.instagram.com/bangtanu20/">Instagram</a>
				</li>
			</ul>
		</div>
		<div class="footer-text">
			<span>Fanpage Shop</span>
			<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fsin.traning%2F%3Fmodal%3Dadmin_todo_tour&tabs&width=300px&height=230px&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="300px" height="230px" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
		</div>
	</div>
	<div class="copyright">
		<div class="wrap">
			<div class="copyright-txt">
				© All rights reserved | Template by SHOESVN
			</div>
		</div>
	</div>
</div>